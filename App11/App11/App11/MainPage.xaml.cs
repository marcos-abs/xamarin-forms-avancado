﻿using App11.Traducao;
using Xamarin.Forms;

/*
 * Fonte do curso Xamarin Avançado:
 * D:\Udemy Vídeos\Xamarin Forms - Avançado\10. App multi-idiomas
 * 6. Tradução Manual - Parte 3 - Dependency Service.mp4
 * 
 * Fonte do Plugin:
 * https://github.com/xamarin/xamarin-forms-samples/tree/master/TodoLocalized
 */

namespace App11 {
    public partial class MainPage : ContentPage {
        public MainPage() {
            InitializeComponent();

            //Lang.AppLang.Culture = new System.Globalization.CultureInfo("pt-PT");
            Lang.AppLang.Culture = DependencyService.Get<ILocale>().GetCurrentCultureInfo();
            lblMsg.Text = Lang.AppLang.MSG_01;
        }
    }
}
