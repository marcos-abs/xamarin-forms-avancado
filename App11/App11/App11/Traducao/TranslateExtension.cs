﻿using System;
using Xamarin.Forms.Xaml;
using Xamarin.Forms;
using System.Diagnostics;

/* Fonte do site:
 * https://github.com/xamarin/xamarin-forms-samples/blob/master/TodoLocalized/TodoLocalized/ViewsXaml/TranslateExtension.cs
 * https://github.com/xamarin/xamarin-forms-samples/blob/master/TodoLocalized/TodoLocalized/L10n.cs
 * 
 * Fonte da aula em vídeo:
 * D:\Udemy Vídeos\Xamarin Forms - Avançado\10. App multi-idiomas
 * 7. Tradução Manual - Parte 4 - XAML.mp4
 */

namespace App11.Traducao
{
    // You exclude the 'Extension' suffix when using in Xaml markup
    [ContentProperty("Text")]
    public class TranslateExtension : IMarkupExtension {
        public string Text { get; set; }

        public object ProvideValue(IServiceProvider serviceProvider) {
            if (Text == null)
                return null;
            Debug.WriteLine("Provide: " + Text);
            // Do your translation lookup here, using whatever method you require
            var translated = L10n.Localize(Text, Text);

            return translated;
        }
    }
}
