﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly:ExportRenderer(typeof(App4.Customs.MyButton), typeof(App4.Droid.MyButtonRenderer))]
namespace App4.Droid {
#pragma warning disable CS0618 // Type or member is obsolete
    public class MyButtonRenderer : ButtonRenderer {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e) {
            base.OnElementChanged(e);
            if (Control != null) {
                Control.SetBackgroundResource(Resource.Drawable.button_round);
            }
        }
    }
#pragma warning restore CS0618 // Type or member is obsolete
}