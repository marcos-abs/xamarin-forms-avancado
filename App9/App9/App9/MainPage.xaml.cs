﻿using Xamarin.Forms;

namespace App9 {
    public partial class MainPage : ContentPage {
        public MainPage() {
            InitializeComponent();

            lblNome.Text = Lang.AppLang.LBL_NOME;
            txtNome.Placeholder = Lang.AppLang.TXT_PH_NOME;
            btnSalvar.Text = Lang.AppLang.BTN_SALVAR;
        }
    }
}
