﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App8
{
	public partial class MainPage : ContentPage
	{
        int count = 0;
		public MainPage()
		{
			InitializeComponent();

            // Tipos de Gesture:
            // a) Tap = Toque ou clique;
            // b) Pinch = Movimento de pinça ou aumentar ou mesmo zoom;
            // c) Pan = Movimento de selecionar sem soltar e arrastar.

            PanGestureRecognizer pan = new PanGestureRecognizer();
            pan.PanUpdated += PanGestureRecognizer_Panned;
            MyLabel.GestureRecognizers.Add(pan);
		}

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e) {
            count++;
            //DisplayAlert("Tapped", count == 1 ? $"Label foi clicada {count} vez" : $"Label foi clicada {count} vezes", "Ok");
            //MyBox.TranslateTo(200, 350, 1000, Easing.BounceOut);
            //MyBox.ScaleTo(2, 1000);
            //MyBox.FadeTo(0.5, 1000);
            //MyBox.RotateTo(45, 1000, Easing.SpringOut);
            var anim = new Animation(v => MyBox.WidthRequest = v, 50, 100);
            anim.Commit(this, "animação", 16, 1000);
        }

        private void PanGestureRecognizer_Panned(object sender, PanUpdatedEventArgs e) {

            if(e.StatusType == GestureStatus.Running) {
                Rectangle rect = new Rectangle(e.TotalX, e.TotalY, 200, 50);
                AbsoluteLayout.SetLayoutBounds(MyLabel, rect);
                AbsoluteLayout.SetLayoutFlags(MyLabel, AbsoluteLayoutFlags.None);

                MyLabel.Text = "x=" + e.TotalX.ToString() + " y=" + e.TotalY.ToString();
            }
        }
    }
}
