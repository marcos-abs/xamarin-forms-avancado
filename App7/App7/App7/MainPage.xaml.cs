﻿using Xamarin.Forms;

namespace App7 {
    public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();

            if(Device.Idiom == TargetIdiom.Tablet || Device.Idiom == TargetIdiom.Desktop) {
                Container.BackgroundColor = Color.Aqua;
            }

#pragma warning disable CS0618 // Type or member is obsolete
            Device.OnPlatform(iOS: () => {
                    Thickness Margin = new Thickness(0, 10, 0, 0);
                },
                Android: () => {
                    Thickness Margin = new Thickness(0, 0, 0, 0);
                },
                WinPhone: () => {
                    Thickness Margin = new Thickness(0, 0, 0, 0);
                }
            );
#pragma warning restore CS0618 // Type or member is obsolete
        }
	}
}
