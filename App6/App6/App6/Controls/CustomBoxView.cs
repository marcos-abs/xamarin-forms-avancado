﻿using Xamarin.Forms;

namespace App6.Controls {
    public class CustomBoxView : BoxView {
#pragma warning disable CS0618 // Type or member is obsolete
        public static readonly BindableProperty EspessuraProperty = BindableProperty.Create<CustomBoxView, double>(
            p=>p.Espessura, default(double)
            );
#pragma warning restore CS0618 // Type or member is obsolete

        public double Espessura {
            get { return (double)GetValue(EspessuraProperty);  }
            set { SetValue(EspessuraProperty, value);  }
        }
    }
}
