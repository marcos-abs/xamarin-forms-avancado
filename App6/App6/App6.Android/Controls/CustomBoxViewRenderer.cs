﻿using System.ComponentModel;
using Android.Graphics;
using App6.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using System.Drawing;

[assembly: ExportRenderer(typeof(CustomBoxView), typeof(App6.Droid.Controls.CustomBoxViewRenderer))]
namespace App6.Droid.Controls {
#pragma warning disable CS0618 // Type or member is obsolete
    public class CustomBoxViewRenderer : BoxRenderer {
        public CustomBoxViewRenderer() {
            SetWillNotDraw(false); // alterar default que é não desenhar.
        }
        public override void Draw(Canvas canvas) {
            base.Draw(canvas);

            CustomBoxView control = (CustomBoxView)Element;

            Paint p = new Paint();

            p.StrokeWidth = (float)control.Espessura;
            p.Color = Android.Graphics.Color.Black;
            p.SetStyle(Paint.Style.Stroke);

            Rect rect = new Rect(0, 0, 200, 200);

            canvas.DrawRect(rect, p); // retangulo preto

            canvas.DrawLine(100, 0, 100, 200, p); // primeira linha

            canvas.DrawLine(0, 100, 200, 100, p); // segunda linha
        }
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e) {
            base.OnElementPropertyChanged(sender, e);
            Invalidate();
        }
    }
#pragma warning restore CS0618 // Type or member is obsolete
}